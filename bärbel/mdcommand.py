import logging
from nio import AsyncClient, MatrixRoom, RoomMessageText
import os

from bärbel.chat_functions import send_text_to_room
from bärbel.config import Config
from bärbel.storage import Storage

logger = logging.getLogger(__name__)


class MdCommand:
    def __init__(self, config: Config):
        """ generic class to send random phrase from file"""
        self.config = config
        # Todo get folder from Config
        self.txtFolder = "./bärbel/md_commands"
        self.commands = {}
        # load commands
        for file in os.listdir(self.txtFolder):
            if file.endswith(".md"):
                command = os.path.splitext(file)[0]
                logger.debug(
                    f"found command {command} -> {os.path.join(self.txtFolder, file)}"
                )
                with open(os.path.join(self.txtFolder, file), "r") as command_file:
                    content = command_file.read()
                    self.commands[command] = content

    def getCommandlist(self):
        """ Returns a list of all the comands that are availiable"""
        return self.commands.keys()

    async def process_message(self, client: AsyncClient, store: Storage,
                              command: str, room: MatrixRoom,
                              event: RoomMessageText):
        """Process the command and send response"""
        try:
            action = command.split(' ')[0]
            content = self.commands[action]
            await send_text_to_room(client, room.room_id, content)
        except Exception as e:
            logger.error("Could not process")
            logger.error(e)
