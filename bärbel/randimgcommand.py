import logging
from random import randrange
from nio import AsyncClient, MatrixRoom, RoomMessageText
import os

from bärbel.chat_functions import send_image_to_room
from bärbel.config import Config
from bärbel.storage import Storage

logger = logging.getLogger(__name__)


class RandImgCommand:
    def __init__(self, config: Config):
        """ generic class to send random phrase from file"""
        self.config = config
        # Todo get folder from Config
        self.folder = "./bärbel/imgPhrase"
        self.commands = {}
        # load commands
        for foldername in os.listdir(self.folder):
            if os.path.isdir(os.path.join(self.folder, foldername)):
                command = foldername
                files = []
                for file in os.listdir(os.path.join(self.folder, foldername)):
                    # check if images are there
                    if file.endswith(".jpg") or file.endswith(".png"):
                        logger.debug(
                            f"found image {file} -> {os.path.join(self.folder, foldername, file)}"
                        )
                        files.append(os.path.join(self.folder, foldername,
                                                 file))
                if len(files) != 0:
                    self.commands[command] = files

    def getCommandlist(self):
        """ Returns a list of all the comands that are availiable"""
        return self.commands.keys()

    async def process_message(self, client: AsyncClient, store: Storage,
                              command: str, room: MatrixRoom,
                              event: RoomMessageText):
        """Process the command and send response"""
        try:
            action = command.split(' ')[0]
            file_list = self.commands[action]
            imgfile = file_list[randrange(len(file_list))]
            await send_image_to_room(client, room.room_id, imgfile)
        except Exception as e:
            logger.error("Could not process")
            logger.error(e)
