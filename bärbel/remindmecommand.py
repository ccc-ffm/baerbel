import logging
from nio import AsyncClient, MatrixRoom, RoomMessageText

from bärbel.chat_functions import send_text_to_room
from bärbel.config import Config
from bärbel.storage import Storage
import asyncio

logger = logging.getLogger(__name__)

class RemindMeCommand:
    def __init__(self, config: Config):
        """ generic class to remind user after time"""
        self.config = config
        self.commands = ['remindMe', 'remindme', 'r', 'reminder']
        self.loop = asyncio.get_event_loop()

    def getCommandlist(self):
        """ Returns a list of all the comands that are availiable"""
        return self.commands

    async def run_later(self, sec, coro):
        await asyncio.sleep(sec)
        return await coro

    async def sendReminder(self, client=None, room_id=None, content=None):
        try:
            await send_text_to_room(client, room_id, content)
        except Exception as e:
            logger.error("Could not process")
            logger.error(e)

    def parseTime(self, timestr):
        """ parses a string like <x>d|<x>h|<x>m|<x>s|<x>
            and returns the amount of secounds"""
        if timestr[len(timestr)-1] not in ['d', 'h', 'm', 's']:
            return int(timestr)
        if timestr[len(timestr)-1] == 'd':
            timestr = str(int(timestr[:len(timestr)-1])*24)+'h'
        if timestr[len(timestr)-1] == 'h':
            timestr = str(int(timestr[:len(timestr)-1])*60)+'m'
        if timestr[len(timestr)-1] == 'm':
            timestr = str(int(timestr[:len(timestr)-1])*60)+'s'
        if timestr[len(timestr)-1] == 's':
            timestr = int(timestr[:len(timestr)-1])
            return timestr
        return int(timestr)


    async def process_message(self, client: AsyncClient, store: Storage,
                              command: str, room: MatrixRoom,
                              event: RoomMessageText):
        """Process the command and send response"""
        try:
            parts = command.split(' ')
            command = parts[0]
            timeToWait = parts[1]
            reminder = parts[2]
            try:
                sec = self.parseTime(timeToWait)
                self.loop.create_task(self.run_later(sec,
                        self.sendReminder(client=client,room_id=room.room_id,content="Reminder: "+reminder)))
            except Exception as e:
                raise e
            await send_text_to_room(client, room.room_id, "Reminder is set")
        except Exception as e:
            logger.error("Could not process")
            logger.error(e)
