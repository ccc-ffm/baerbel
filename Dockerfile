FROM ubuntu:20.04 as base

ENV PYROOT /pyroot
ENV PYTHONUSERBASE $PYROOT

RUN apt-get update && \
    apt-get install --yes \
    ca-certificates \
    libolm3 \
    python3 \
    python3-setuptools && \
    apt-get clean


FROM base as builder

RUN apt-get update && \
    apt-get --yes install \
        libolm-dev \
        pipenv \
        python3-dev \
        python3-pip

COPY Pipfile* ./

RUN PIP_USER=1 PIP_IGNORE_INSTALLED=1 pipenv install --system --deploy --ignore-pipfile


FROM base as baerbel

COPY --from=builder $PYROOT/bin/ $PYROOT/bin/
COPY --from=builder $PYROOT/lib/ $PYROOT/lib/

WORKDIR /app

COPY . .

CMD ["python3", "run_bärbel"]
